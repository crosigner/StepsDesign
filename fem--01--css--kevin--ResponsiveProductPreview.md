# fem ProductPreview kevin
![](images/2023-04-29-01-00-56.png)
repo  
![](images/2023-04-29-01-16-02.png)

style guide  
![](images/2023-04-29-01-07-27.png)
![](images/2023-04-29-01-07-38.png)
![](images/2023-04-29-01-08-46.png)

html  
![](images/2023-04-29-00-58-58.png)
![](images/2023-04-29-01-14-29.png)
![](images/2023-04-29-01-18-36.png)

flex-group  
![](images/2023-04-29-01-19-23.png)

custom css reset  
![](images/2023-04-29-01-00-29.png)

colors vars  
![](images/2023-04-29-01-09-25.png)

utilities  
![](images/2023-04-29-01-11-03.png)

body  
![](images/2023-04-29-01-10-06.png)
![](images/2023-04-29-01-10-38.png)
![](images/2023-04-29-01-10-16.png)

product  
![](images/2023-04-29-01-12-50.png)
![](images/2023-04-29-01-11-34.png)

.product__content  
![](images/2023-04-29-01-13-06.png)

.product__category  
![](images/2023-04-29-01-13-34.png)

.product__title  
![](images/2023-04-29-01-13-54.png)

.product__price  
![](images/2023-04-29-01-18-09.png)

.visually-hidden:not  
![](images/2023-04-29-01-19-58.png)

.button  
![](images/2023-04-29-01-20-42.png)

.button:is(:hover, :focus)  
![](images/2023-04-29-01-21-12.png)